#guia 3
"""para este ejercicio se trabajará con 3 laboratorios"""

def abrir_archivo(nombre=None):
    archivo = open(nombre, "r")
    return archivo

def escribe_archivo_reporte(texto=None):
    archivo = open("archivo.txt", "w")
    archivo.write(texto)
    archivo.close()


def escribe_archivo_reporte_modo_a(texto):
    reporte = open("reporte-append.txt", "a")
    reporte.write(texto)
    reporte.close()


def trabajar_archivo(archivo):
    codigo = input("indique el codigo del producto: \n")
    nombre = input("ingrese el nombre del farmaco: \n")
    componente = input("ingrese el componente principal: \n")
    precio = input("ingrese el precio de venta: \n")
    
    linea1 = (f"codigo del producto: {codigo}\n")
    linea2 = (f"nombre del farmaco: {nombre}\n")
    linea3 = (f"componente principal: {componente}")
    linea4 = (f"precio de venta: {precio}\n")
    texto = f"{linea1} {linea2} {linea3} {linea4}"
    return texto


def main():
    lab = int(input("escoja el laboratorio para agregar el medicamento\n"))
    if lab == 1:
        try: 
            lab1 = abrir_archivo("lab1.txt")
            texto = trabajar_archivo(lab1)
            escribe_archivo_reporte_modo_a(texto)
        except FileNotFoundError:
            lab1 = open("lab1.txt", "w")
            lab1.close()
            texto = trabajar_archivo(lab1)
            escribe_archivo_reporte_modo_a(texto)

    elif lab == 2:
        try: 
            lab2 = abrir_archivo("lab2.txt")
            texto = trabajar_archivo(lab2)
            escribe_archivo_reporte_modo_a(texto)
        except FileNotFoundError:
            lab2 = open("lab2.txt", "w")
            lab2.close()
            texto = trabajar_archivo(lab2)
            escribe_archivo_reporte_modo_a(texto)

    elif lab == 3:
        try: 
            lab3 = abrir_archivo("lab3.txt")
            texto = trabajar_archivo(lab3)
            escribe_archivo_reporte_modo_a(texto)
        except FileNotFoundError:
            lab3 = open("lab3.txt", "w")
            lab3.close()
            texto = trabajar_archivo(lab3)
            escribe_archivo_reporte_modo_a(texto)


if __name__ == "__main__":
    main()
